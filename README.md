# certificates

###### To create auto signed certificate trsueted in local, we can use https://github.com/FiloSottile/mkcert:

mkcert localhost 127.0.0.1

###### Format des certificats
**Format PEM** : the most use
Le format le plus utilisé valable pour les certificats et les clés privées. La plupart des serveurs (par exemple Apache) utilisent la clé privée et le certificat dans les fichiers séparés. Des certificats PEM sont au format texte codé en Base64.
PEM est un fichier codé en Base64 utilisant des caractères ASCII.
Les extensions sont généralement .cer, .crt, .pem ou .key (pour la clé privée).
Utilisent les serveurs Apache et autres serveurs sous Unix/Linux OS.

**Format DER**
DER est un format de certificat binaire. Ce n'est pas un fichier texte et par conséquent, il ne peut pas être édité, ouvert et copié en tant que texte en Base64 (dans le Notepad etc.).
Tous les types de certificats et les clés privées peuvent être sauvegardés au format DER.
Les extensions sont généralement .cer ou .der.
Utilisent les plates-formes Java.
Format P7B/PKCS#7
Le format P7B est un format basé sur le Base64. Les extensions sont .p7b ou .p7c.

**Un fichier P7B** 
contient le certificat et des certificats de chaîne (certificats intermédiaires) sans la clé privée.
Utilisent les plate-formes Java Tomcat.
Format PFX/P12/PKCS#12
Le format PKCS#12 ou .pfx/P12 est un format binaire et contient le certificat (et son intermédiaire) et la clé privée. Le fichier .pfx est protégé par un mot de passe.

**Les extensions sont .pfx et .p12.**
Utilisé sous Windows pour importer et exporter le certificat avec la clé privée.
Les fichiers PFX sont également utilisés pour la signature dans Microsoft Authenticode.


###### To test the correct functionality you could simply use curl after the creation of pkcs12 certificate chain including private key and certificate. In the curl option, after certificate file indication, you must specify the certificate passphrase you’ve used during client certificate preparation.

openssl pkcs12 -export -inkey client.key -in client.crt -name test-x509-client-side -out client.p12

###### Remove passphrase

openssl rsa -in server.key -out serversp.key

###### Read certificate present by a host
openssl s_client -showcerts -connect hosturl -prexit

###### Read certificate content 
openssl x509 -in certificate.pem -text

 openssl x509 -in MYCERT.der -inform der -text
